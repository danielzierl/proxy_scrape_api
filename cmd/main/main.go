package main

import (
	"fmt"
	"net/http"
	"time"

	"gitlab.com/danielzierl/proxy_scrape_api/internal/handlers"
	"gitlab.com/danielzierl/proxy_scrape_api/internal/network"
	"gitlab.com/danielzierl/proxy_scrape_api/internal/proxy_util"
	"gitlab.com/danielzierl/proxy_scrape_api/internal/storage"
)

func main() {

	go func() {
		for {
			fmt.Println("check")
			var proxies []string
			proxies = network.Get_proxyscape_proxies("http")
			proxies = append(proxies, network.Get_proxyscape_proxies("socks4")...)
			proxies = append(proxies, network.Get_proxyscape_proxies("socks5")...)
			storage.Write_to_posix_timestamp_file(proxies)
			new_filenames := storage.Get_all_files_from_dir_not_older_than("cache", 90*time.Minute)
			fmt.Printf("New files count: %d\n", len(new_filenames))
			proxies_merged := proxy_util.Merge_proxy_files(new_filenames, "cache")
			storage.Write_to_file("merged.txt", proxies_merged)

			time.Sleep(300 * time.Second)

		}
	}()
	port := 8080
	var use_tls bool = false
	fmt.Printf("Starting server on port %d\n", port)
	http.HandleFunc("/get-proxies", handlers.GetProxiesHandler)
	http.HandleFunc("/ping", handlers.Ping)
	certFile := ""
	keyFile := ""

	var err_bind error
	if use_tls {
		err_bind = http.ListenAndServeTLS(fmt.Sprintf("0.0.0.0:%d", port), certFile, keyFile, nil)
	} else {
		err_bind = http.ListenAndServe(fmt.Sprintf("0.0.0.0:%d", port), nil)
	}
	if err_bind != nil {
		panic(err_bind)
	}

}
