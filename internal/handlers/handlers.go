package handlers

import (
	"net/http"
	"strings"

	"gitlab.com/danielzierl/proxy_scrape_api/internal/storage"
)

func GetProxiesHandler(w http.ResponseWriter, r *http.Request) {
	proxies := storage.Get_proxies_from_file("merged.txt", ".")
	w.Header().Set("Content-Type", "text/plain")
	w.Write([]byte(strings.Join(proxies, "\n")))

}
func Ping(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/plain")
	w.Write([]byte("pong"))
}
