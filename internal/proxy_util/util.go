package proxy_util

import (
	"fmt"

	"gitlab.com/danielzierl/proxy_scrape_api/internal/storage"
)

func filter_proxies(proxies []string) []string {
	// create seen set
	seen := make(map[string]bool)
	for _, proxy := range proxies {
		if !seen[proxy] {
			seen[proxy] = true
		}
	}
	// convert set to slice
	proxies = make([]string, 0, len(seen))
	for proxy := range seen {
		proxies = append(proxies, proxy)
	}
	return proxies
}
func Merge_proxy_files(proxy_files []string, dir string) []string {
	proxies := make([]string, 0)
	var total_proxies int = 0
	for _, proxy_file := range proxy_files {
		file_proxies := storage.Get_proxies_from_file(proxy_file, dir)
		total_proxies += len(file_proxies)
		proxies = append(proxies, file_proxies...)

	}
	proxies = filter_proxies(proxies)
	fmt.Printf("Filtered proxies %d -> %d\n", total_proxies, len(proxies))
	return proxies
}
