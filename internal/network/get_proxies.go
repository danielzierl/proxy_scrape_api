package network

import (
	"io"
	"net/http"
	"strings"
)

func Get_proxyscape_proxies(protocol string) []string {
	url := "https://api.proxyscrape.com/v2/?request=displayproxies&protocol=" + protocol + "&timeout=10000&timeout=10000&country=all&ssl=all&anonymity=all"
	resp, err := http.Get(url)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	proxies := strings.Split(string(body), "\n")
	for i, proxy := range proxies {
		proxies[i] = protocol + "://" + proxy
	}
	return proxies

}
