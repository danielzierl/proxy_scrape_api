package storage

import (
	"os"
	"strconv"
	"time"
)

func Get_all_files_from_dir_not_older_than(dir string, threshold time.Duration) []string {
	files, err := os.ReadDir(dir)
	var out []string = make([]string, 0)
	if err != nil {
		panic(err)
	}
	for _, file := range files {
		timestamp, err := strconv.ParseInt(file.Name(), 10, 64)
		if err != nil {
			panic(err)
		}
		if time.Since(time.Unix(timestamp, 0)) > threshold {
			continue
		}
		out = append(out, file.Name())

	}
	return out
}
