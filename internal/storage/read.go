package storage

import (
	"os"
	"strings"
)

func Get_proxies_from_file(file_name string, dir string) []string {
	contents, err := os.ReadFile(dir + "/" + file_name)
	if err != nil {
		panic(err)
	}
	proxies := strings.Split(string(contents), "\n")
	return proxies
}
