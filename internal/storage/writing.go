package storage

import (
	"fmt"
	"os"
	"strings"
	"time"
)

func Write_to_posix_timestamp_file(file_data []string) {
	timestamp := time.Now().Unix()
	_, err := os.Stat("cache")
	if err != nil {
		os.Mkdir("cache", 0755)
	}

	file_name := fmt.Sprintf("cache/%d", timestamp)
	err = os.WriteFile(file_name, []byte(strings.Join(file_data, "\n")), 0644)
	if err != nil {
		panic(err)
	}
}
func Write_to_file(file_name string, file_data []string) {
	err := os.WriteFile(file_name, []byte(strings.Join(file_data, "\n")), 0644)
	if err != nil {
		panic(err)
	}
}
